package main

import (
	"fmt"
)

func main() {
	fmt.Println("This is a test app for the boilerplate gitlab-ci.yaml project.")
	fmt.Println("https://gitlab.com/chriseaton/gitlab-ci-example-go")
	fmt.Println()
}