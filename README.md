# [Gitlab Continuous Integration: Go Boilerplate](https://gitlab.com/chriseaton/gitlab-ci-example-go)

This file configures GitLab's Continuous Integration service to vet, build, test, and deploy most Go projects. It's
setup to be generic enough for projects, and easy to pick up and understand just by examination.

## Instructions
Just drop this project's ".gitlab-ci.yml" file into your project's root source directory and add it to your git repository.
````
git add .gitlab-ci.yml
````

Once pushed to GitLab and every time you push your source code, the build & test scripts will fire, and GitLab will 
provide details under the Build section of your project.
````
git commit -m "Added Gitlab continuous integration!"
git push
````